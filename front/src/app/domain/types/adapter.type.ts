
export interface AdapterSettings {
    adapter: string;
    proxyEnable: boolean;
    proxyProtocol?: 'http' | 'https';
    proxyLogin?: string;
    proxyPassword?: string;
    proxyHost?: string;
    proxyPort?: number;
}

export interface AdminerSettings extends AdapterSettings {
    server: string;
    url: string;
    login: string;
    password: string;
}

export interface PhpMyAdmin511Settings extends AdapterSettings {
    url: string;
}
