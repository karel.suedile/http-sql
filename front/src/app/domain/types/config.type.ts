import type { AdapterSettings } from './adapter.type';

export type QuerierPane = {
    id: string;
    name: string;
    sql: string;
};

export type ServerConfig = {
    id: string;
    name: string;
    activeDatabase?: string;
    settings: AdapterSettings;
    activeQuerierId?: string;
    queriers: QuerierPane[];
};

export type Config = {
    activeServerId?: string;
    servers: ServerConfig[];
};
