import type { QuerierPane } from '../types/config.type';

export class QuerierModel {
    public id: string;
    public name: string;
    public sql: string;

    public constructor(config: QuerierPane) {
        this.name = config.name;
        this.sql = config.sql;
        this.id = config.id;
    }
}
