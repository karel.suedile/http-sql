import type { ServerConfig } from '../types/config.type';
import { QuerierModel } from './querier.model';
import type { AdapterSettings } from '../types/adapter.type';

export class ServerModel<T extends AdapterSettings = AdapterSettings> {
    private activeQuerier: QuerierModel | undefined;
    private queriers: QuerierModel[] = [];

    public readonly id: string;
    public name: string;
    public activeDatabase?: string;

    public db: AdapterSettings;
    public autocomplete: Record<string, string[]> = {};

    public constructor(config: ServerConfig) {
        this.id = config.id;
        this.queriers = config.queriers?.map(querierConfig => new QuerierModel(querierConfig)) || [];
        this.activeQuerier = this.queriers.find(q => q.id === config.activeQuerierId);
        this.name = config.name;
        this.activeDatabase = config.activeDatabase;
        this.db = config.settings;
    }

    public complete(word: string): string[] {
        return this.autocomplete[word] || [];
    }

    public getTablesCompletion(): string[] {
        return Object.keys(this.autocomplete);
    }

    public addQuerier(querier: QuerierModel): void {
        this.queriers = [...this.queriers, querier];
    }

    public removeQuerier(querier: QuerierModel): void {
        this.queriers = this.queriers.filter(q => q !== querier);
    }

    public setActiveQuerier(querier: QuerierModel | undefined): void {
        this.activeQuerier = querier;
    }

    public getActiveQuerier(): QuerierModel | undefined {
        return this.activeQuerier;
    }

    public getQueriers(): QuerierModel[] {
        return this.queriers;
    }

}
