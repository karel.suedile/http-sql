import { CompletionHelper } from './completion.helper';

describe('Completion Helper', () => {
    it('should with alias 1/2', () => {
        const query = 'SELECT * FROM users u, orders o LIMIT 5';
        const result = CompletionHelper.getTablesWithAlias(query);
        expect(result).toEqual({"o": "orders", "u": "users"});
    });

    it('should with alias 2/2', () => {
        const query = 'SELECT * FROM users AS u, orders AS o LIMIT 5';
        const result = CompletionHelper.getTablesWithAlias(query);
        expect(result).toEqual({"o": "orders", "u": "users"});
    });

    it('should without alias', () => {
        const query = 'SELECT * FROM users, orders LIMIT 5';
        const result = CompletionHelper.getTablesWithAlias(query);
        expect(result).toEqual({"orders": "orders", "users": "users"});
    });

    it('should mix', () => {
        const query = 'SELECT * FROM users AS u, orders o, store LIMIT 5';
        const result = CompletionHelper.getTablesWithAlias(query);
        expect(result).toEqual({"o": "orders", "u": "users", "store": "store"});
    });

    it('should mix with precoce end', () => {
        const query = 'SELECT * FROM users AS u, orders o, store';
        const result = CompletionHelper.getTablesWithAlias(query);
        expect(result).toEqual({"o": "orders", "u": "users", "store": "store"});
    });

    it('should be void', () => {
        const query = 'SELECT 1';
        const result = CompletionHelper.getTablesWithAlias(query);
        expect(result).toEqual({});
    });

    it('should mix with join', () => {
        const query = 'SELECT * FROM users AS u LEFT JOIN orders o ON (o.userId=u.id) JOIN store ON store.id=o.storeId LIMIT 5';
        const result = CompletionHelper.getTablesWithAlias(query);
        expect(result).toEqual({"o": "orders", "u": "users", "store": "store"});
    });

    it('should with backquotes', () => {
        const query = 'SELECT * FROM `users` AS u, `orders` AS o LIMIT 5';
        const result = CompletionHelper.getTablesWithAlias(query);
        expect(result).toEqual({"o": "orders", "u": "users"});
    });
});
