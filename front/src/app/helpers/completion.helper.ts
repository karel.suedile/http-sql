
export class CompletionHelper {
    private static mysqlKeywords: string[] = [
        "ADD","ALL","ALTER","ANALYZE","AND","AS","ASC","ASENSITIVE","BEFORE",
        "BETWEEN","BIGINT","BINARY","BLOB","BOTH","BY","CALL","CASCADE","CASE",
        "CHANGE","CHAR","CHARACTER","CHECK","COLLATE","COLUMN","CONDITION",
        "CONSTRAINT","CONTINUE","CONVERT","CREATE","CROSS","CURRENT_DATE",
        "CURRENT_TIME","CURRENT_TIMESTAMP","CURRENT_USER","CURSOR","DATABASE",
        "DATABASES","DAY_HOUR","DAY_MICROSECOND","DAY_MINUTE","DAY_SECOND","DEC",
        "DECIMAL","DECLARE","DEFAULT","DELAYED","DELETE","DESC","DESCRIBE",
        "DETERMINISTIC","DISTINCT","DISTINCTROW","DIV","DOUBLE","DROP","DUAL",
        "EACH","ELSE","ELSEIF","ENCLOSED","ESCAPED","EXISTS","EXIT","EXPLAIN",
        "FALSE","FETCH","FLOAT","FLOAT4","FLOAT8","FOR","FORCE","FOREIGN",
        "FROM","FULLTEXT","GENERATED","GET","GRANT","GROUP","HAVING",
        "HIGH_PRIORITY","HOUR_MICROSECOND","HOUR_MINUTE","HOUR_SECOND","IF",
        "IGNORE","IN","INDEX","INFILE","INNER","INOUT","INSENSITIVE","INSERT",
        "INT","INT1","INT2","INT3","INT4","INT8","INTEGER","INTERVAL","INTO",
        "IO_AFTER_GTIDS","IO_BEFORE_GTIDS","IS","ITERATE","JOIN","KEY","KEYS",
        "KILL","LEADING","LEAVE","LEFT","LIKE","LIMIT","LINEAR","LINES","LOAD",
        "LOCALTIME","LOCALTIMESTAMP","LOCK","LONG","LONGBLOB","LONGTEXT","LOOP",
        "LOW_PRIORITY","MASTER_BIND","MASTER_SSL_VERIFY_SERVER_CERT","MATCH",
        "MAXVALUE","MEDIUMBLOB","MEDIUMINT","MEDIUMTEXT","MIDDLEINT",
        "MINUTE_MICROSECOND","MINUTE_SECOND","MOD","MODIFIES","NATURAL","NOT",
        "NO_WRITE_TO_BINLOG","NULL","NUMERIC","ON","OPTIMIZE","OPTION",
        "OPTIONALLY","OR","ORDER","OUT","OUTER","OUTFILE","PARTITION","PRECISION",
        "PRIMARY","PROCEDURE","PURGE","RANGE","READ","READS","READ_WRITE",
        "REAL","REFERENCES","REGEXP","RELEASE","RENAME","REPEAT","REPLACE",
        "REQUIRE","RESTRICT","RETURN","REVOKE","RIGHT","RLIKE","ROW","ROW_COUNT",
        "SCHEMA","SCHEMAS","SECOND_MICROSECOND","SELECT","SENSITIVE","SEPARATOR",
        "SET","SHOW","SMALLINT","SPATIAL","SPECIFIC","SQL","SQLEXCEPTION",
        "SQLSTATE","SQLWARNING","SQL_BIG_RESULT","SQL_CALC_FOUND_ROWS",
        "SQL_SMALL_RESULT","SSL","STARTING","STRAIGHT_JOIN","TABLE","TERMINATED",
        "THEN","TINYBLOB","TINYINT","TINYTEXT","TO","TRAILING","TRIGGER","TRUE",
        "UNDO","UNION","UNIQUE","UNLOCK","UNSIGNED","UPDATE","USAGE","USE",
        "USING","UTC_DATE","UTC_TIME","UTC_TIMESTAMP","VALUES","VARBINARY",
        "VARCHAR","VARCHARACTER","VARYING","WHEN","WHERE","WHILE","WITH"].map(s => s.toLowerCase());

    private static selectKeywords = ['SELECT', 'FROM', 'WHERE', 'GROUP BY', 'HAVING', 'ORDER BY', 'LIMIT'].map(s => s.toLowerCase());

    public static getTablesWithAlias(sql: string): Record<string, string> {
        const tables: Record<string, string> = {};
        const fromIndex = sql.toLowerCase().indexOf('from ');
        const words = sql.toLowerCase().slice(fromIndex)
            .replace(/,/g, ' join ')
            .replace(/\n/g, ' ')
            .replace(/\s+/g, ' ')
            .replace(/^from/, 'join')
            .replace(/ as /g, ' ')
            .replace(/`/g, '')
            .split(' ');
        while (words.length > 0) {
            words.shift();
            const word = words.shift();
            if (!word) {
                break;
            }
            const alias = words.shift();

            if (alias && !CompletionHelper.mysqlKeywords.includes(alias) && alias.match(/^[A-Za-z0-9]+$/)) {
                // prendre l'alias en compte
                tables[alias] = word;
            } else {
                // ne pas prendre l'alias en compte
                tables[word] = word;
                tables['`' + word + '`'] = word;
                if (!alias) {
                    break;
                }
                words.unshift(alias);
            }

            let next: string | undefined = words.shift();
            let breaker: boolean = false;
            while (next !== 'join' && words.length > 0) {
                next = words.shift();
                if (CompletionHelper.selectKeywords.includes(word)) {
                    breaker = true;
                    break;
                }
            }

            if (breaker) {
                break;
            } else if (next) {
                words.unshift(next);
            }
        }
        return tables;
    }
}
