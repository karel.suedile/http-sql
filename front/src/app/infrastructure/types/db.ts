
export type DbConf = {
    adapter: string;
    server: string;
    url: string;
    login: string;
    password: string;
    proxyEnable: boolean;
    proxyProtocol?: 'http' | 'https';
    proxyLogin?: string;
    proxyPassword?: string;
    proxyHost?: string;
    proxyPort?: number;
};
