import type { SqlResponse } from '../types/sql-response';
import type { DbConf } from '../types/db';

type IRequest = {
    method: 'GET' | 'POST';
    headers: Record<string, string>;
    payload?: string;
    url: string;
    proxy?: {
        protocol: 'http' | 'https';
        login?: string;
        password?: string;
        host: string;
        port: number;
    };
};

type IResponse = {
    headers: Record<string, string>;
    payload: string;
};

export abstract class DbAbstractRepository {


    protected readonly serverConfig: DbConf;

    public constructor(serverConfig: DbConf) {
        this.serverConfig = serverConfig;
    }

    protected async call(body: IRequest): Promise<IResponse> {
        if (this.serverConfig.proxyEnable) {
            body.proxy = {
                protocol: this.serverConfig.proxyProtocol || 'http',
                login: this.serverConfig.proxyLogin,
                password: this.serverConfig.proxyPassword,
                host: this.serverConfig.proxyHost || '',
                port: this.serverConfig.proxyPort || 0
            };
        }
        const response = await fetch('http://127.0.0.1:3000/', {
            headers: { 'content-type': 'application/json' },
            method: 'POST',
            body: JSON.stringify(body)
        });
        return await response.json();
    }

    public abstract login(): Promise<boolean>;
    public abstract execSql(sql: string): Promise<SqlResponse>;
    public abstract getDatabases(): Promise<string[]>;
    public abstract useDatabase(database: string): Promise<boolean>;
    public abstract getCurrentDatabase(): Promise<string>;
    public abstract getTables(database?: string): Promise<string[]>;
    public abstract getAutocompletion(): Promise<Record<string, string[]>>;
}
