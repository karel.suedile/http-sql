import { DbAbstractRepository } from './db-abstract.repository';
import type { SqlResponse } from '../types/sql-response';

export class AdminerRepository extends DbAbstractRepository {
    private cookie: string = '';
    private csrf: string = '';
    private currentDatabase: string = '';

    public async login(): Promise<boolean> {
        try {
            const json = await this.call({
                method: 'POST',
                headers: {
                    'content-type': 'application/x-www-form-urlencoded'
                },
                payload: 'auth%5Bdriver%5D=server&auth%5Bserver%5D=' + encodeURIComponent(this.serverConfig.server) +
                    '&auth%5Busername%5D=' + encodeURIComponent(this.serverConfig.login) +
                    '&auth%5Bpassword%5D=' + encodeURIComponent(this.serverConfig.password) +'&auth%5Bdb%5D=',
                url: this.serverConfig.url + '?server=' + encodeURIComponent(this.serverConfig.server)
            });
            this.cookie = json.headers['set-cookie']?.split('; ').find(f => f.startsWith('adminer_sid=')) || '';
            this.csrf = await this.getCsrf();
        } catch (e) {
            return false;
        }
        return true;
    }

    public async execSql(sql: string): Promise<SqlResponse> {
        const json = await this.call({
            method: 'POST',
            headers: {
                'content-type': 'multipart/form-data; boundary=----WebKitFormBoundaryU9gwGxlmf80wtHLv',
                'Cookie': this.cookie
            },
            url: this.serverConfig.url + '?server=' + encodeURIComponent(this.serverConfig.server) +
                '&username=' + encodeURIComponent(this.serverConfig.login) +
                (this.currentDatabase ? '&db=' + encodeURIComponent(this.currentDatabase) : '') +
                '&sql='+ encodeURIComponent(sql),
            payload: '------WebKitFormBoundaryU9gwGxlmf80wtHLv\r\n'+
                'Content-Disposition: form-data; name="query"\r\n'+
                '\r\n'+
                sql + '\r\n'+
                '------WebKitFormBoundaryU9gwGxlmf80wtHLv\r\n'+
                'Content-Disposition: form-data; name="limit"\r\n'+
                '\r\n'+
                '\r\n'+
                '------WebKitFormBoundaryU9gwGxlmf80wtHLv\r\n'+
                'Content-Disposition: form-data; name="token"\r\n'+
                '\r\n'+
                this.csrf + '\r\n'+
                '------WebKitFormBoundaryU9gwGxlmf80wtHLv--\r\n'
        });

        this.csrf = this.parseCsrf(json.payload);

        // /<p class='error'>(.(?!hede).)*<form
        const error = json.payload.match(/<p class='error'>((?:(?!<form).)*)<form/s);
        if (error) {
            if (error.length > 1) {
                return [{ error: error[1].replace(/\n/g, '') }];
            }
            return [{ error: 'unknown error: ' + JSON.stringify(error) }];
        }

        const table = json.payload.match(/<table[^>]*>(.*)<\/table>/s)?.[0] || '';
        return this.tableToStruct(table);
    }

    public async getDatabases(): Promise<string[]> {
        const json = await this.execSql('show databases');
        return json.map(entry => entry.Database);
    }

    public async useDatabase(database: string): Promise<boolean> {
        this.currentDatabase = database;
        await this.execSql('use ' + database);
        return true;
    }

    public async getTables(database?: string): Promise<string[]> {
        const json = await this.execSql('show tables');
        return json.map(entry => Object.values(entry)[0]);
    }

    public async getCurrentDatabase(): Promise<string> {
        return this.currentDatabase;
    }

    public async getAutocompletion(): Promise<Record<string, string[]>> {
        const completions: Record<string, string[]> = {};
        const json = await this.execSql('select TABLE_NAME, COLUMN_NAME from information_schema.`COLUMNS`');
        for (const item of json) {
            if (!completions[item.TABLE_NAME]) {
                completions[item.TABLE_NAME] = [];
            }
            completions[item.TABLE_NAME].push(item.COLUMN_NAME);
        }
        Object.keys(completions).forEach(key => {
            completions[key] = [...new Set(completions[key])];
        });
        return completions;
    }

    private parseCsrf(payload: string): string {
        return payload.match(/<input type='hidden' name='token' value='([^']+)'>/)?.[1] || '';
    }

    private async getCsrf(): Promise<string> {
        const json = await this.call({
            method: 'GET',
            headers: {
                'content-type': 'application/x-www-form-urlencoded',
                'Cookie': this.cookie
            },
            url: this.serverConfig.url + '?server=' + encodeURIComponent(this.serverConfig.server) +
                '&username=' + encodeURIComponent(this.serverConfig.login) +
                '&db=' + encodeURIComponent(this.currentDatabase)
        });
        return this.parseCsrf(json.payload);
    }

    private tableToStruct(htmlTable: string): SqlResponse {
        const root = document.createElement('div');
        root.innerHTML = htmlTable;
        const keys: string[] = [];
        const items: SqlResponse = [];

        [...root.querySelectorAll('tr')].forEach((row, rowIndex) => {
            if (rowIndex === 0) {
                [...row.querySelectorAll('th')].forEach((el, index) => {
                    keys.push(el.innerText);
                });
            } else {
                const item: Record<string, string> = {};
                [...row.querySelectorAll('td')].forEach((el, index) => {
                    item[keys[index]] = el.innerText;
                });
                items.push(item);
            }
        });
        return items;
    }
}
