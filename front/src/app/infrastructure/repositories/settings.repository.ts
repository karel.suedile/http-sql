import type { Config, ServerConfig } from '../../domain/types/config.type';
import type { ServerModel } from '../../domain/models/server.model';

export class SettingsRepository {

    private readonly storageKey = 'http-sql';

    private config: Config;

    public constructor(
        private readonly storage: Storage
    ) {
        const config = this.storage.getItem(this.storageKey);
        this.config = config ? JSON.parse(config) : { servers: [] };
    }

    public getServers(): ServerConfig[] {
        return this.config.servers;
    }

    public setActiveServerId(id: string | undefined): void {
        this.config.activeServerId = id;
    }

    public getActiveServerId(): string | undefined {
        return this.config.activeServerId;
    }

    public save(servers: ServerModel[], activeServerId: string | undefined): void {
        const config: Config = {
            activeServerId: activeServerId,
            servers: servers.map(server => {
                return {
                    id: server.id,
                    name: server.name,
                    settings: server.db,
                    activeQuerierId: server.getActiveQuerier()?.id,
                    activeDatabase: server.activeDatabase,
                    queriers: server.getQueriers().map(querier => {
                        return {
                            id: querier.id,
                            name: querier.name,
                            sql: querier.sql
                        };
                    })
                };
            })
        };
        this.config = config;
        const serialized = JSON.stringify(config);
        this.storage.setItem(this.storageKey, serialized);
    }
}
