import { DbAbstractRepository } from './db-abstract.repository';
import type { SqlResponse } from '../types/sql-response';

export class Phpmyadmin511Repository extends DbAbstractRepository {
    private cookie: string = '';
    private token: string = '';
    private currentDatabase: string = '';

    public async login(): Promise<boolean> {
        try {
            const json = await this.call({
                method: 'GET',
                headers: {},
                url: this.serverConfig.url + '/'
            });
            this.cookie = json.headers['set-cookie']?.split('; ').find(f => f.startsWith('phpMyAdmin_https=')) || '';
            this.token = this.parseToken(json.payload);
        } catch (e) {
            return false;
        }
        return true;
    }

    public async execSql(sql: string): Promise<SqlResponse> {
        const json = await this.call({
            method: 'POST',
            headers: {
                'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'Cookie': this.cookie
            },
            url: this.serverConfig.url + '/index.php?route=/import',
            payload: 'db=' + encodeURIComponent(this.currentDatabase) +
                '&token=' + this.token +
                '&sql_query=' + encodeURIComponent(sql) +
                '&ajax_request=true'
        });

        // this.token = this.parseToken(json.payload);

        const res = JSON.parse(json.payload);
        const error = res.error?.match(/<ol><li>(.*)(?:(?!<\/li).)*<\/li/s);
        if (error) {
            if (error.length > 1) {
                return [{ error: error[1].replace(/\n/g, '') }];
            }
            return [{ error: 'unknown error: ' + JSON.stringify(error) }];
        }

        const table = res.message.match(/<table[^>]*>(.*)<\/table>/s)?.[0] || '';
        return this.tableToStruct(table);
    }

    public async getDatabases(): Promise<string[]> {
        const json = await this.execSql('show databases');
        return json.map(entry => entry.Database);
    }

    public async useDatabase(database: string): Promise<boolean> {
        this.currentDatabase = database;
        await this.execSql('use ' + database);
        return true;
    }

    public async getTables(database?: string): Promise<string[]> {
        const json = await this.execSql('show tables');
        return json.map(entry => Object.values(entry)[0]);
    }

    public async getCurrentDatabase(): Promise<string> {
        return this.currentDatabase;
    }

    public async getAutocompletion(): Promise<Record<string, string[]>> {
        const completions: Record<string, string[]> = {};
        const json = await this.execSql('select TABLE_NAME, COLUMN_NAME from information_schema.`COLUMNS` LIMIT 1000000');
        for (const item of json) {
            if (!completions[item.TABLE_NAME]) {
                completions[item.TABLE_NAME] = [];
            }
            completions[item.TABLE_NAME].push(item.COLUMN_NAME);
        }
        Object.keys(completions).forEach(key => {
            completions[key] = [...new Set(completions[key])];
        });
        return completions;
    }

    private parseToken(payload: string): string {
        return payload.match(/<input type="hidden" name="token" value="([^"]+)">/)?.[1] || '';
    }

    private tableToStruct(htmlTable: string): SqlResponse {
        const root = document.createElement('div');
        root.innerHTML = htmlTable;
        const keys: string[] = [];
        const items: SqlResponse = [];

        [...root.querySelectorAll('tr')].forEach((row, rowIndex) => {
            if (rowIndex === 0) {
                [...row.querySelectorAll('th')].forEach((el, index) => {
                    keys.push(el.innerText.replace(/\n/g, ''));
                });
            } else {
                const item: Record<string, string> = {};
                [...row.querySelectorAll('td')].forEach((el, index) => {
                    item[keys[index]] = el.innerText;
                });
                items.push(item);
            }
        });
        return items;
    }
}
