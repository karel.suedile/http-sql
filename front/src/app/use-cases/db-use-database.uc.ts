import type { ServerModel } from '../domain/models/server.model';
import type { SettingsRepository } from '../infrastructure/repositories/settings.repository';
import type { DbAbstractRepository } from '../infrastructure/repositories/db-abstract.repository';
import useCases from './index';

export class DbUseDatabaseUc {

    public constructor(
        private readonly settings: SettingsRepository
    ) { }

    public async execute(server: ServerModel, database: string): Promise<boolean> {
        const repository: DbAbstractRepository = useCases.dbGetRepositoryUc.execute(server);
        const result = await repository.useDatabase(database);
        if (result) {
            server.activeDatabase = database;
        }
        return result;
    }
}
