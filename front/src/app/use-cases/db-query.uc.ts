import type { ServerModel } from '../domain/models/server.model';
import type { SettingsRepository } from '../infrastructure/repositories/settings.repository';
import type { DbAbstractRepository } from '../infrastructure/repositories/db-abstract.repository';
import useCases from './index';
import type { SqlResponse } from '../infrastructure/types/sql-response';

export class DbQueryUc {

    public constructor(
        private readonly settings: SettingsRepository
    ) { }

    public execute(server: ServerModel, sql: string): Promise<SqlResponse> {
        const repository: DbAbstractRepository = useCases.dbGetRepositoryUc.execute(server);
        return repository.execSql(sql);
    }
}
