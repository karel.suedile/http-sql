import type { ServerModel } from '../domain/models/server.model';
import type { SettingsRepository } from '../infrastructure/repositories/settings.repository';
import type { DbAbstractRepository } from '../infrastructure/repositories/db-abstract.repository';
import { AdminerRepository } from '../infrastructure/repositories/adminer.repository';
import type { DbConf } from '../infrastructure/types/db';
import { Phpmyadmin511Repository } from '../infrastructure/repositories/phpmyadmin5-1-1.repository';

export class DbGetRepositoryUc {

    private repositories: Map<ServerModel, DbAbstractRepository> = new Map<ServerModel, DbAbstractRepository>();

    public constructor(
        private readonly settings: SettingsRepository
    ) { }

    public execute(server: ServerModel): DbAbstractRepository {
        let repository: DbAbstractRepository | undefined = this.repositories.get(server);
        if (repository) {
            return repository;
        }

        switch (server.db.adapter) {
            case 'adminer':
                repository = new AdminerRepository(server.db as DbConf);
                break;
            case 'phpmyadmin511':
                repository = new Phpmyadmin511Repository(server.db as DbConf);
                break;
        }
        if (!repository) {
            throw new Error('No repository found for adapter: ' + server.db.adapter);
        }
        this.repositories.set(server, repository);
        return repository;
    }
}
