import type { SettingsRepository } from '../infrastructure/repositories/settings.repository';

export class GetActiveServerUc {

    public constructor(
        private readonly settings: SettingsRepository
    ) { }

    public execute(): string | undefined {
        return this.settings.getActiveServerId();
    }
}
