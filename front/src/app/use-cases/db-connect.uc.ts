import type { ServerModel } from '../domain/models/server.model';
import type { SettingsRepository } from '../infrastructure/repositories/settings.repository';
import type { DbAbstractRepository } from '../infrastructure/repositories/db-abstract.repository';
import useCases from './index';

export class DbConnectUc {

    public constructor(
        private readonly settings: SettingsRepository
    ) { }

    public async execute(server: ServerModel): Promise<boolean> {
        const repository: DbAbstractRepository = useCases.dbGetRepositoryUc.execute(server);
        if (!await repository.login()) {
            return false;
        }
        server.autocomplete = await repository.getAutocompletion();
        if (server.activeDatabase) {
            return repository.useDatabase(server.activeDatabase);
        }
        return true;
    }
}
