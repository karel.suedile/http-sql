import type { ServerModel } from '../domain/models/server.model';
import type { SettingsRepository } from '../infrastructure/repositories/settings.repository';
import { QuerierModel } from '../domain/models/querier.model';
import type { QuerierPane } from '../domain/types/config.type';

export class CreateQuerierUc {

    public constructor(
        private readonly settings: SettingsRepository
    ) { }

    public execute(server: ServerModel): QuerierModel {
        const conf: QuerierPane = {
            id: crypto.randomUUID(),
            name: Date.now().toString(),
            sql: ''
        };
        const querier = new QuerierModel(conf);
        server.addQuerier(querier);
        server.setActiveQuerier(querier);
        return querier;
    }
}
