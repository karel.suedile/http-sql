import type { ServerModel } from '../domain/models/server.model';
import type { SettingsRepository } from '../infrastructure/repositories/settings.repository';
import type { DbAbstractRepository } from '../infrastructure/repositories/db-abstract.repository';
import useCases from './index';

export class DbFetchDatabasesUc {

    public constructor(
        private readonly settings: SettingsRepository
    ) { }

    public execute(server: ServerModel): Promise<string[]> {
        const repository: DbAbstractRepository = useCases.dbGetRepositoryUc.execute(server);
        return repository.getDatabases();
    }
}
