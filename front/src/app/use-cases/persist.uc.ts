import type { ServerModel } from '../domain/models/server.model';
import type { SettingsRepository } from '../infrastructure/repositories/settings.repository';
import useCases from './index';

export class PersistUc {

    public constructor(
        private readonly settings: SettingsRepository
    ) { }

    public execute(servers: ServerModel[]): void {
        const activeServerId = useCases.getActiveServerUc.execute();
        this.settings.save(servers, activeServerId);
    }
}
