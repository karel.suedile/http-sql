import { ServerModel } from '../domain/models/server.model';
import type { SettingsRepository } from '../infrastructure/repositories/settings.repository';
import type { ServerConfig } from '../domain/types/config.type';

export class CreateServerUc {

    public constructor(
        private readonly settings: SettingsRepository
    ) { }

    public execute(type: string): ServerModel {
        const conf: ServerConfig = {
            id: crypto.randomUUID(),
            settings: {
                adapter: type,
                proxyEnable: false
            },
            name: Date.now().toString(),
            queriers: []
        };
        return new ServerModel(conf);
    }
}
