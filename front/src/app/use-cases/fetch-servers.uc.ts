import { ServerModel } from '../domain/models/server.model';
import type { SettingsRepository } from '../infrastructure/repositories/settings.repository';

export class FetchServersUc {

    public constructor(
        private readonly settings: SettingsRepository
    ) { }

    public execute(): ServerModel[] {
        const confServers = this.settings.getServers();
        return confServers.map(conf => new ServerModel(conf));
    }
}
