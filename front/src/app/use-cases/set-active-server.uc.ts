import type { ServerModel } from '../domain/models/server.model';
import type { SettingsRepository } from '../infrastructure/repositories/settings.repository';

export class SetActiveServerUc {

    public constructor(
        private readonly settings: SettingsRepository
    ) { }

    public execute(server: ServerModel | undefined): void {
        this.settings.setActiveServerId(server?.id);
    }
}
