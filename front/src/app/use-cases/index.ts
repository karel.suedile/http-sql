import { FetchServersUc } from './fetch-servers.uc';
import { SettingsRepository } from '../infrastructure/repositories/settings.repository';
import { GetActiveServerUc } from './get-active-server.uc';
import { SetActiveServerUc } from './set-active-server.uc';
import { CreateServerUc } from './create-server.uc';
import { CreateQuerierUc } from './create-querier.uc';
import { DbGetRepositoryUc } from './db-get-repository.uc';
import { DbConnectUc } from './db-connect.uc';
import { DbUseDatabaseUc } from './db-use-database.uc';
import { DbQueryUc } from './db-query.uc';
import { PersistUc } from './persist.uc';
import { FakeStorageModel } from '../domain/models/fake-storage.model';
import { DbFetchDatabasesUc } from './db-fetch-databases.uc';

export class UseCases {
    public fetchServersUc: FetchServersUc;
    public getActiveServerUc: GetActiveServerUc;
    public setActiveServerUc: SetActiveServerUc;
    public createServerUc: CreateServerUc;
    public createQuerierUc: CreateQuerierUc;
    public dbGetRepositoryUc: DbGetRepositoryUc;
    public dbConnectUc: DbConnectUc;
    public dbUseDatabaseUc: DbUseDatabaseUc;
    public dbFetchDatabasesUc: DbFetchDatabasesUc;
    public dbQueryUc: DbQueryUc;
    public persistUc: PersistUc;

    public constructor() {
        let storage: Storage = new FakeStorageModel();
        if (typeof window !== 'undefined' && typeof document !== 'undefined') {
            storage = localStorage;
        }
        const settings = new SettingsRepository(storage);
        this.fetchServersUc = new FetchServersUc(settings);
        this.getActiveServerUc = new GetActiveServerUc(settings);
        this.setActiveServerUc = new SetActiveServerUc(settings);
        this.createServerUc = new CreateServerUc(settings);
        this.createQuerierUc = new CreateQuerierUc(settings);
        this.dbGetRepositoryUc = new DbGetRepositoryUc(settings);
        this.dbConnectUc = new DbConnectUc(settings);
        this.dbUseDatabaseUc = new DbUseDatabaseUc(settings);
        this.dbFetchDatabasesUc = new DbFetchDatabasesUc(settings);
        this.dbQueryUc = new DbQueryUc(settings);
        this.persistUc = new PersistUc(settings);
    }
}

export default new UseCases();
