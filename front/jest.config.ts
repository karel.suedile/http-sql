
const config = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    verbose: true,
    resetMocks: true,
    restoreMocks: true,
    collectCoverage: true,
    setupFiles: ["./jest-setup-file.ts"],
    moduleFileExtensions: [
        'js',
        'json',
        'ts'
    ],
    testResultsProcessor: 'jest-sonar-reporter',
    collectCoverageFrom: [
        'src/modules/**/{use-cases,domain}/**/*.ts',
        '!src/modules/**/*.spec-mock.ts'
    ],
    coverageReporters: ['text-summary', 'html', 'lcov', 'cobertura'],
    rootDir: '',
    testMatch: [
        '**/__tests__/**/*.+(ts|tsx|js)',
        '**/?(*.)+(spec|test).+(ts|tsx|js)'
    ],
    coverageDirectory: './coverage',
    transform: {
        '^.+\\.(ts|tsx)$': 'ts-jest'
    },
    reporters: [ 'default' ]
};

export default config;
// module.exports = config;
