# Http-SQL

A GUI tool to execute SQL queries through Web Interface such as Adminer, PhpMyAdmin.

## Concept

Sometimes it is not possible to use our prefered SQL tool like MySQL Workbench, Sequel Ace, ...
Instead, we are given a web interface like Adminer, which is "limited".
Indeed, such web interface gives no possibility of storing query or do some auto-completion.

So, the goal of Http-SQL, is to provide the possibilities of a Sequel Ace tool through a remote connection.
In order to achieve this, instead of connecting through an SSH tunnel to the SQL server port,
Http-SQL connects through http protocol of the web interface.

## How to use

### Global installation

The project is using **node 18**.

- git clone https://gitlab.com/karel.suedile/http-sql.git
- cd http-sql

### Backend

A backend server is required in order to proxy request. The frontend cannot execute direct http request because of the CORS.

To run it :
- cd back
- npm ci
- npm run start:dev

It should create a server listening to port 3000

### Frontend :

The GUI

- cd front
- npm ci
- npm run dev

It should create a server listening on a specific url you should find in the command log.

Open the link and enjoy !
