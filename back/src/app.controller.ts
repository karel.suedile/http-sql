import { BadRequestException, Body, Controller, Post } from '@nestjs/common';
import axios, { AxiosHeaders, AxiosResponseHeaders, RawAxiosResponseHeaders } from 'axios';
import { HttpsProxyAgent } from 'hpagent';

interface IRequest<T> {
    method: string;
    headers: AxiosHeaders;
    payload: T;
    url: string;
    proxy?: {
        protocol: 'http' | 'https';
        login?: string;
        password?: string;
        host: string;
        port: number;
    };
}

interface IResponse {
    status: number;
    headers: RawAxiosResponseHeaders | AxiosResponseHeaders;
    payload: string;
}

@Controller()
export class AppController {
    @Post()
    public async getHello<T, V>(@Body() body: IRequest<V>): Promise<IResponse> {
        let httpsAgent: HttpsProxyAgent;
        if (body.proxy) {
            let proxy = body.proxy.protocol + '://';
            if (body.proxy.login) {
                proxy += encodeURIComponent(body.proxy.login);
                if (body.proxy.password) {
                    proxy += ':' + encodeURIComponent(body.proxy.password);
                }
                proxy += '@';
            }
            proxy += body.proxy.host + ':' + body.proxy.port;

            httpsAgent = new HttpsProxyAgent({
                proxy,
                rejectUnauthorized: false
            });
        }

        try {
            const response = await axios.request<T>({
                method: body.method,
                data: body.payload,
                headers: body.headers,
                url: body.url,
                httpsAgent,
                maxRedirects: 0,
                validateStatus: function (status) {
                    return status >= 200 && status < 400; // default
                }
            });
            return {
                status: response.status,
                headers: response.headers,
                payload: typeof response.data === 'string' ? response.data : JSON.stringify(response.data)
            };
        } catch (e) {
            throw new BadRequestException({ cause: e });
        }
    }
}
